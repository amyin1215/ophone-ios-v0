//
//  MenuViewController.m
//  OTracks
//
//  Created by Amy Yin on 02/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "MenuViewController.h"
#import "Composition.h"
#import "CompositionsViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (!self.palette) {
        Palette *defaultPalette = [[Palette alloc] init];
        defaultPalette.name = @"Coffee";
        defaultPalette.id = 1;
        self.palette = defaultPalette;
    }
    self.paletteLabel.text = self.palette.name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CompositionDetailsViewControllerDelegate

- (void)compositionDetailsViewControllerDidCancel: (CompositionDetailsViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)compositionDetailsViewControllerDidSave:
    (CompositionDetailsViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)compositionDetailsViewController: (CompositionDetailsViewController *)controller
                       didAddComposition:(Composition *)composition
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self performSegueWithIdentifier:@"ExistingCompositions" sender:composition];
}

- (void)compositionDetailsViewController: (CompositionDetailsViewController *)controller
                      didEditComposition:(Composition *)composition
{
    NSLog(@"Unreachable--should not be able to edit composition from MenuViewController");
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"AddComposition"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        CompositionDetailsViewController *compositionDetailsViewController = [navigationController viewControllers][0];
        compositionDetailsViewController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"ExistingCompositions"]) {
        NSLog(@"Menu Existing Compositions Segue");
        // TODO: Send composition to Existing Compositions for addition to list
        
        CompositionsViewController *destViewController = segue.destinationViewController;
        destViewController.palette = self.palette;
        NSLog(@"MenuViewController: %@", self.palette.name);
    }
}

- (UICollectionReusableView *)collectionView: (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Collection Reusable View");
    ScentHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                   UICollectionElementKindSectionHeader withReuseIdentifier:@"ScentHeaderView" forIndexPath:indexPath];
    // TODO: set Palette name
    NSString *searchTerm = @"Palette Name"; //self.searches[indexPath.section];
    [headerView setSearchText:searchTerm];
    return headerView;
}

@end
