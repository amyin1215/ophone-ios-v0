//
//  ScentsCollectionViewController.m
//  OTracks
//
//  Created by Amy Yin on 06/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "ScentsCollectionViewController.h"
#import "Scent.h"

@interface ScentsCollectionViewController ()

@end

@implementation ScentsCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.scents = [NSMutableArray arrayWithCapacity:20];
    Scent *scent = [[Scent alloc] init];
    scent.sname = @"Croissant";
    scent.id = 1;
    [self.scents addObject:scent];
    
    scent = [[Scent alloc] init];
    scent.sname = @"Espresso";
    scent.id = 2;
    [self.scents addObject:scent];
    
    scent = [[Scent alloc] init];
    scent.sname = @"Wine";
    scent.id = 3;
    [self.scents addObject:scent];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
