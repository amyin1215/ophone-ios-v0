//
//  AppDelegate.h
//  OTracks
//
//  Created by Amy Yin on 08/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
