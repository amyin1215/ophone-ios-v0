//
//  Constants.m
//  OTracks
//
//  Created by Amy Yin on 22/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "Constants.h"

NSString *const ErrorPaletteName = @"No Palettes Found.";
const int ErrorPaletteID = -1;
