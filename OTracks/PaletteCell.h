//
//  PaletteCell.h
//  OTracks
//
//  Created by Amy Yin on 15/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
@class Palette;
@interface PaletteCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *name;
@property (nonatomic, strong) Palette * palette;
@end