//
//  Palette.m
//  OTracks
//
//  Created by Amy Yin on 15/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import "PaletteCell.h"
#import "Palette.h"
#import "Constants.h"

@implementation PaletteCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setPalette:(Palette *)palette {
    
    if(_palette != palette) {
        _palette = palette;
    }
    
    self.imageView.image = [self getPaletteImage:palette];
    self.name.text = [palette.name capitalizedString];
}

- (UIImage *) getPaletteImage: (Palette *) palette {
    
    if (palette.id == ErrorPaletteID) {
        return [UIImage imageNamed:@"error"];
    }
    return [palette.name isEqual: @"coffee"] ? [UIImage imageNamed:@"palette.png"] : [UIImage imageNamed:@"Food_fruit_tart.gif"];
}

@end
