//
//  ScentCell.h
//  OTracks
//
//  Created by Amy Yin on 15/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScentCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *sname;
@property NSInteger *id;

@end
