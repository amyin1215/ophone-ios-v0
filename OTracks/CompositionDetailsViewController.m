//
//  CompositionDetailsViewController.m
//  OTracks
//
//  Created by Amy Yin on 01/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "CompositionDetailsViewController.h"
#import "Composition.h"

@interface CompositionDetailsViewController ()

@end

@implementation CompositionDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Blank Composition";
    if (self.compositionToView != nil) {
        self.title = [NSString stringWithFormat:@"Viewing %@", self.compositionToView.title];
        self.titleTextField.text = self.compositionToView.title;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    [self.delegate compositionDetailsViewControllerDidCancel:self];
    NSLog(@"cancel");
}
- (IBAction)done:(id)sender {
    if (self.compositionToView != nil) {
        NSLog(@"compositionToView not nil");
        self.compositionToView.title = self.titleTextField.text;
        [self.delegate compositionDetailsViewController:self didEditComposition:self.compositionToView];
    } else {
        Composition *composition = [[Composition alloc] init];
        composition.title = self.titleTextField.text;
        composition.id = 99;
        [self.delegate compositionDetailsViewController:self
                                  didAddComposition:composition];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        NSLog(@"init CompositionDetailsViewController"); }
    return self; }
- (void)dealloc {
    NSLog(@"dealloc CompositionDetailsViewController"); }

@end
