//
//  Composition.h
//  OTracks
//
//  Created by Amy Yin on 15/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import "Palette.h"
#import <Foundation/Foundation.h>

@interface Composition : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSMutableArray *scents;
@property (nonatomic) Palette *palette;

@end
