//
//  Scent.h
//  OTracks
//
//  Created by Amy Yin on 06/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Scent : NSObject
@property (strong, nonatomic) NSString *sname;
@property NSInteger id; // TODO: create hashtable of scent ids to bluetooth signal
@end
