//
//  Palette.h
//  OTracks
//
//  Created by Amy Yin on 17/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Palette : NSObject

@property (strong, nonatomic) NSString *name;
@property int id;
@property (strong, atomic) NSArray *scents; // four scents per palette
@property (strong, nonatomic) NSString *description;

@end
