//
//  ScentHeaderView.h
//  OTracks
//
//  Created by Amy Yin on 06/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScentHeaderView : UICollectionReusableView
-(void)setSearchText:(NSString *)text;
@end
