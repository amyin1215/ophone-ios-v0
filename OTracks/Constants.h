//
//  Constants.h
//  OTracks
//
//  Created by Amy Yin on 22/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ErrorPaletteName;

FOUNDATION_EXPORT const int ErrorPaletteID;
