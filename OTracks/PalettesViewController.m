//
//  PalettesViewController.m
//  OTracks
//
//  Created by Amy Yin on 17/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//
//  Inspired by http://www.raywenderlich.com/22324/beginning-uicollectionview-in-ios-6-part-12
//

#import "PalettesViewController.h"
#import "Palette.h"
#import "OTracks.h"
#import "PaletteCell.h"
#import "Constants.h"
#import "PalettesHeaderView.h"

@interface PalettesViewController () <UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, weak) IBOutlet UITextField *textField;
@property(nonatomic, strong) NSMutableDictionary *searchResults;
@property(nonatomic, strong) NSMutableArray *searches;
@property(nonatomic, strong) OTracks *OTracks;

@end

@implementation PalettesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"four_palettes.png"]];
    self.searches = [@[] mutableCopy];
    self.searchResults = [@{} mutableCopy];
    self.OTracks = [[OTracks alloc] init];
    
    UIImage *textFieldImage = [[UIImage imageNamed:@"search_field.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self.textField setBackground:textFieldImage];
    
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"PaletteCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate methods
- (BOOL) textFieldShouldReturn:(UITextField *)textField {

    [self.OTracks searchOTracksForPalette:textField.text completionBlock:^(NSString *searchTerm, NSArray *results, NSError *error) {
        NSString *downloaded = @"Palettes from OTracks";
        if(results && [results count] > 0) {
            
            if(![self.searches containsObject:downloaded]) {
                NSLog(@"Found %d photos matching %@", [results count],searchTerm);
                [self.searches insertObject:downloaded atIndex:0];
            }
            self.searchResults[downloaded] = results;
            
        } else {
            NSLog(@"Error searching for palettes: %@", error.localizedDescription);
            
            // TODO: Put proper "No results found" message into collection view
            
            if(![self.searches containsObject:downloaded]) {
                [self.searches insertObject:downloaded atIndex:0];
            }
            Palette *empty_palette = [[Palette alloc] init];
            empty_palette.name = ErrorPaletteName;
            empty_palette.id = ErrorPaletteID;
            self.searchResults[downloaded] = [[NSArray alloc]initWithObjects:empty_palette, nil];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
        });
    
    }];
    [textField resignFirstResponder];
    return YES; 
}

- (NSArray *) searchForPalette:(NSString *)term completionBlock:(id)block {
    Palette *palette1 = [[Palette alloc] init];
    palette1.name = @"Coffee";
    palette1.id = 1;
    
    Palette *palette2 = [[Palette alloc] init];
    palette2.name = @"Fruittart";
    palette2.id = 2;
    
    return @[palette1, palette2];

    
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    NSString *searchTerm = self.searches[section];
    return [self.searchResults[searchTerm] count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return [self.searches count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PaletteCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"PaletteCell" forIndexPath:indexPath];
    NSString *searchTerm = self.searches[indexPath.section];
    cell.palette = self.searchResults[searchTerm]
    [indexPath.row];
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

- (UICollectionReusableView *)collectionView: (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    PalettesHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                         UICollectionElementKindSectionHeader withReuseIdentifier:@"PalettesHeaderView" forIndexPath:indexPath];
    NSString *searchTerm = self.searches[indexPath.section];
    [headerView setSectionHeader:searchTerm];
    return headerView;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //NSString *searchTerm = self.searches[indexPath.section];
    //Palette * palette = self.searchResults[searchTerm][indexPath.row];
    CGSize retval = CGSizeMake(70, 100);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 5, 5, 5);
    // _, left, _ , _
}

@end
