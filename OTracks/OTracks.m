//
//  OTracks.m
//  OTracks
//
//  Created by Amy Yin on 20/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//
// Adapted from http://www.raywenderlich.com/22324/beginning-uicollectionview-in-ios-6-part-12
//

#import "OTracks.h"
#import "Palette.h"

#define baseURL @"ophone.fr/gcm_server_php"

@implementation OTracks

+ (NSString *)oTracksSearchURLForPalette:(NSString *) searchTerm
{
    searchTerm = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:@"http://%@/get_palettes.php/?palette_search=%@", baseURL, searchTerm];
}

- (void)searchOTracksForPalette:(NSString *) term completionBlock:(PaletteSearchCompletionBlock) completionBlock
{
    NSString *searchURL = [OTracks oTracksSearchURLForPalette:term];
   
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSString *searchResultString = [NSString stringWithContentsOfURL:[NSURL URLWithString:searchURL]
                                                                encoding:NSUTF8StringEncoding
                                                                   error:&error];
        if (error != nil) {
            NSLog(@"Check Internet Connection.");
            completionBlock(term,nil,error);
        }
        else
        {
            // Parse the JSON Response
            NSData *jsonData = [searchResultString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                              options:kNilOptions
                                                                                error:&error];
            if(error != nil)
            {
                NSLog(@"Error parsing JSON Response.");
                completionBlock(term,nil,error);
                if (searchResultsDict == nil) {
                    // TODO: Alert that no palettes found matching search term
                    NSError * error = [[NSError alloc] initWithDomain:@"OTracks" code:0 userInfo:@{NSLocalizedFailureReasonErrorKey: @"No search results matching term."}];
                    completionBlock(term, nil, error);
                }
            }
            else
            {
                NSMutableArray *palettes = [@[] mutableCopy];
                for(NSMutableDictionary *objPalette in searchResultsDict)
                {
                    Palette *palette = [[Palette alloc] init];
                    palette.id = [objPalette[@"id"] intValue];
                    palette.name = objPalette[@"name"];
                    palette.description = objPalette[@"description"];
                    
                    [palettes addObject:palette];
                }
                
                completionBlock(term,palettes,nil);
            }
        }
    });
}

//+ (void)loadImageForPhoto:(FlickrPhoto *)flickrPhoto thumbnail:(BOOL)thumbnail completionBlock:(FlickrPhotoCompletionBlock) completionBlock
//{
//    
//    NSString *size = thumbnail ? @"m" : @"b";
//    
//    NSString *searchURL = [Flickr flickrPhotoURLForFlickrPhoto:flickrPhoto size:size];
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    
//    dispatch_async(queue, ^{
//        NSError *error = nil;
//        
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:searchURL]
//                                                  options:0
//                                                    error:&error];
//        if(error)
//        {
//            completionBlock(nil,error);
//        }
//        else
//        {
//            UIImage *image = [UIImage imageWithData:imageData];
//            if([size isEqualToString:@"m"])
//            {
//                flickrPhoto.thumbnail = image;
//            }
//            else
//            {
//                flickrPhoto.largeImage = image;
//            }
//            completionBlock(image,nil);
//        }
//        
//    });
//}



@end
