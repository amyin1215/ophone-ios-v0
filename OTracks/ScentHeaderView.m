//
//  ScentHeaderView.m
//  OTracks
//
//  Created by Amy Yin on 06/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "ScentHeaderView.h"

@interface ScentHeaderView ()
@property(weak) IBOutlet UIImageView *backgroundImageView;
@property(weak) IBOutlet UILabel *searchLabel;
@end

@implementation ScentHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setSearchText:(NSString *)text {
    NSLog(@"setSearchText");
    self.searchLabel.text = text;
    UIImage *shareButtonImage = [[UIImage imageNamed:@"Blank-label.jpg"] resizableImageWithCapInsets:
                                 UIEdgeInsetsMake(68, 68, 68, 68)];
    self.backgroundImageView.image = shareButtonImage;
    self.backgroundImageView.center = self.center;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
