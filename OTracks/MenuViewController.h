//
//  MenuViewController.h
//  OTracks
//
//  Created by Amy Yin on 02/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "WelcomeViewController.h"
#import "CompositionDetailsViewController.h"
#import "ScentHeaderView.h"
#import "Palette.h"

@interface MenuViewController : WelcomeViewController <CompositionDetailsViewControllerDelegate>
@property (strong) Palette *palette;
@property (weak, nonatomic) IBOutlet UILabel *paletteLabel;
@end
