//
//  CompositionDetailsViewController.h
//  OTracks
//
//  Created by Amy Yin on 01/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import "PalettesViewController.h"

@class CompositionDetailsViewController;
@class Composition;

@protocol CompositionDetailsViewControllerDelegate <NSObject>

- (void)compositionDetailsViewControllerDidCancel:
    (CompositionDetailsViewController *)controller;

- (void)compositionDetailsViewController:
    (CompositionDetailsViewController *)controller
                              didAddComposition:(Composition *)composition;

- (void)compositionDetailsViewController: (CompositionDetailsViewController *)controller
                      didEditComposition:(Composition *)composition;

@end

@interface CompositionDetailsViewController : PalettesViewController

@property (nonatomic, weak) id <CompositionDetailsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) Composition *compositionToView;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end