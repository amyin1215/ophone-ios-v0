//
//  CompositionsViewController.h
//  OTracks
//
//  Created by Amy Yin on 15/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompositionDetailsViewController.h"
#import "Palette.h"

@interface CompositionsViewController : UITableViewController
    <CompositionDetailsViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *compositions;
@property (nonatomic, strong) Palette *palette;
@end
