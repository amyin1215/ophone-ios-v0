//
//  PalettesHeaderView.h
//  
//
//  Created by Amy Yin on 22/01/2014.
//
//

#import <UIKit/UIKit.h>

@interface PalettesHeaderView : UICollectionReusableView

-(void)setSectionHeader:(NSString *)text;

@end
