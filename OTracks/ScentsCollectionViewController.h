//
//  ScentsCollectionViewController.h
//  OTracks
//
//  Created by Amy Yin on 06/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScentsCollectionViewController : UICollectionViewController
@property (strong, nonatomic) NSMutableArray *scents;
@end
