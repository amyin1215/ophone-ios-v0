//
//  PalettesHeaderView.m
//  
//
//  Created by Amy Yin on 22/01/2014.
//
//

#import "PalettesHeaderView.h"

@interface PalettesHeaderView ()
@property(weak) IBOutlet UIImageView *backgroundImageView;
@property (weak) IBOutlet UILabel *sectionLabel;
@end

@implementation PalettesHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}

-(void)setSectionHeader:(NSString *)text {
    self.sectionLabel.text = text;
    UIImage *shareButtonImage = [[UIImage imageNamed:@"header_bg.png"] resizableImageWithCapInsets:
                                 UIEdgeInsetsMake(68, 68, 68, 68)];
    self.backgroundImageView.image = shareButtonImage;
    self.backgroundImageView.center = self.center;
}

@end
