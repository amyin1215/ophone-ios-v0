//
//  CompositionsViewController.m
//  OTracks
//
//  Created by Amy Yin on 15/12/2013.
//  Copyright (c) 2013 Vapor Communication. All rights reserved.
//

#import "CompositionsViewController.h"
#import "Composition.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Palette.h"

@interface CompositionsViewController ()
@end

@implementation CompositionsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.compositions = [NSMutableArray arrayWithCapacity:20];
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    
	NSString *urlString = [NSString stringWithFormat:@"http://ophone.fr/gcm_server_php/get_compositions.php"];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
	NSDictionary *headers = [NSDictionary dictionaryWithObject:[self userAgent] forKey:@"User-Agent"];
	[request setAllHTTPHeaderFields:headers];
    
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    __block Composition *composition;
    // TODO: Get compositions from Palette
    NSDictionary *params = @{@"palette": @"coffee"};
    [manager POST:@"http://ophone.fr/gcm_server_php/get_compositions.php" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        for (id item in responseObject) {
            composition = [[Composition alloc] init];
            composition.title = [item valueForKeyPath:@"title"];
            composition.id = [[item valueForKey:@"composition_id"] integerValue];
            composition.palette = _palette;
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date = [df dateFromString: [item valueForKey:@"date"]];
            composition.date = date;
            [self.compositions addObject:composition];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.compositions.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error loading compositions: %@", error);
        composition = [[Composition alloc] init];
        composition.title = @"Walk through Paris";
        composition.id = 1;
        [self.compositions addObject:composition];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.compositions.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        
        composition = [[Composition alloc] init];
        composition.title = @"Stroll through the Tuileries";
        composition.id = 2;
        [self.compositions addObject:composition];

        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.compositions.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        
        composition = [[Composition alloc] init];
        composition.title = @"Singing in the Rain";
        composition.id = 3;
        [self.compositions addObject:composition];

        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.compositions.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
    }];
    
    [self.tableView reloadData];
    [self performSelector:@selector(dismiss) withObject:nil];
}

- (NSString *)userAgent
{
	return [NSString stringWithFormat:@"%@/%@ (%@, %@ %@, %@, Scale/%f)",
            [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleIdentifierKey],
            [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey],
            @"unknown",
            [[UIDevice currentDevice] systemName],
            [[UIDevice currentDevice] systemVersion],
            [[UIDevice currentDevice] model],
            ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] ? [[UIScreen mainScreen] scale] : 1.0)];
}

#pragma mark Dismiss Methods Sample

- (void)dismiss {
    [SVProgressHUD dismiss];
}

- (void)compositionDetailsViewController: (CompositionDetailsViewController *)controller
                       didAddComposition:(Composition *)composition
{
    [self.compositions addObject:composition];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow: ([self.compositions count] - 1) inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)compositionDetailsViewController: (CompositionDetailsViewController *)controller
                      didEditComposition:(Composition *)composition
{
    NSUInteger index = [self.compositions indexOfObject:composition];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index
                                                inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)compositionsViewController: (CompositionsViewController *)controller
                       didAddComposition:(Composition *)composition
{
    [self.compositions addObject:composition];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow: ([self.compositions count] - 1) inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CompositionDetailsViewControllerDelegate
- (void)compositionDetailsViewControllerDidCancel: (CompositionDetailsViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddComposition"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        CompositionDetailsViewController *compositionDetailsViewController = [navigationController viewControllers][0];
        compositionDetailsViewController.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"ViewComposition"]) {
        CompositionDetailsViewController *compositionDetailsViewController = segue.destinationViewController;
        compositionDetailsViewController.delegate = self;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        Composition *composition = self.compositions[indexPath.row];
        compositionDetailsViewController.compositionToView = composition;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.compositions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompositionCell"];
    Composition *composition = (self.compositions)[indexPath.row];
    cell.textLabel.text = composition.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"ID = %d", composition.id];
    return cell;
}

@end
