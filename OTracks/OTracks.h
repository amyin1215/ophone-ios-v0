//
//  OTracks.h
//  OTracks
//
//  Created by Amy Yin on 20/01/2014.
//  Copyright (c) 2014 Vapor Communication. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Palette;

typedef void (^PaletteSearchCompletionBlock)(NSString *searchTerm, NSArray *results, NSError *error);

@interface OTracks : NSObject

- (void)searchOTracksForPalette:(NSString *) term completionBlock:(PaletteSearchCompletionBlock) completionBlock;

//+ (NSString *)flickrPhotoURLForFlickrPhoto:(FlickrPhoto *) flickrPhoto size:(NSString *) size;

@end


